function verifForm0() {
	let chaine = document.querySelector("#txt").value;
	if (chaine.length < 2) {
		alert("ERR: 2 caractères minimum");
		return false;
	}else {
		alert("OK: vous avez saisi : \"" + chaine + "\"");
		return true;
	}
}

function verifForm1(f,chaine) {
	if (chaine.length < 2) alert("ERR: 2 caractères minimum");
	else {
		alert("OK: vous avez saisi : \"" + chaine + "\"");
		f.submit();
	}
}

function verifForm2(eltHTML) {
	let chaine = eltHTML.value;
	let formulaire = eltHTML.form;
	verifForm1(formulaire,chaine);
}

function verifForm3(elt) {
	let chaine = (typeof(elt) != "string") ? elt.value : elt;
	
	if (chaine.length < 2) {
		alert("ERR: 2 caractères minimum");
	} else {
		alert("OK: vous avez saisi : \"" + chaine + "\"");
		document.forms[0].submit();
	}
}

function verifForm3(e,elt) {
	let chaine = (typeof(elt) != "string") ? elt.value : elt;
	
	if (chaine.length < 2) {
		alert("ERR: 2 caractères minimum");
		e.preventDefault();
	} else {
		alert("OK: vous avez saisi : \"" + chaine + "\"");
	}
		
}

// Abonnement du bouton id="btnDOM0"
let btnDOM0 = document.getElementById('btnDOM0');

btnDOM0.onclick = function() {return verifForm0();};
//btnDOM0.addEventListener("click", function() {return verifForm0();}, false);
	// ATTENTION : 	l'ajout d'un event "click" sur un type submit n'empêchera 
	//				pas la soumission du formulaire par un "return false"
	// Il faut remplacer l'event "click" via le DOM0 : "btnDOM0.onclick"

// Abonnement du bouton id="btnDOM2"
let btnDOM2 = document.getElementById('btnDOM2');
let txt = document.querySelector("#txt");
let txt2 = document.querySelector("#txt").value;
btnDOM2.addEventListener("click",function(e) {
									verifForm3(e,txt2);
                                 }, false);












